$(function() {

    var EventViewModel = function() {
        this.summary = ko.observable();
        this.startTime = ko.observable();
        this.endTime = ko.observable();
        this.description = ko.observable();
        this.includeUserInfo = ko.observable(false);
    };

    var AppViewModel = function() {
        var self = this;
        self.event = new EventViewModel();
        self.showCalendar = ko.observable(true);
        self.isBusy = ko.observable(false);

        self.closeModal = function() {
            showModal(false);
        }

        self.openModal = function() {
            showModal(true);
        }

        self.keyUp = function(data, evt) {
            if (evt.keyCode === 27 && evt.target === document.getElementById('app-body')) {
                showModal(false);
            }
        }

        self.addEvent = function() {
            var event = ko.toJS(self.event);

            if (self.isBusy()) {
                return;
            }

            self.isBusy(true);
            console.log('Creating event', event);

            $.ajax({
                method: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                data: JSON.stringify(event),
                url: '/calendar/addEvent' + window.location.search
            }).done(function(data) {
                console.log('Event created.', data);

                toastr.success('Event created.');
                showModal(false);
                self.showCalendar(false);
                self.isBusy(false);
                self.event.summary('');
                self.event.description('');
                self.event.includeUserInfo(false);

                window.setTimeout(function() {
                    self.showCalendar(true);
                }, 500);
            }).fail(function(xhr, status, error) {
                console.log('Error adding event.', error);
                self.isBusy(false);
                var reason = xhr.status === 400 ? xhr.responseText : 'Please try again later.';
                toastr.error('Failed to create event: ' + reason);
            });
        }
    }

    var showModal = function(show) {
        $('#overlay').toggleClass('c-overlay', !!show);
        $('.event-modal').toggleClass('event-modal-hide', !show);
    }

    var vm = new AppViewModel();
    var defaultStart = new Date();
    defaultStart.setMinutes(0);
    defaultStart.setSeconds(0);

    $('#startDate').datetimepicker({
        startDate: defaultStart,
        onChangeDateTime: function(dp) {
            vm.event.startTime(dp && dp.getTime());
          }
    });

    $('#endDate').datetimepicker({
        startDate: defaultStart,
        onChangeDateTime: function(dp) {
            vm.event.endTime(dp && dp.getTime());
          }
    });

    ko.applyBindings(vm, document.getElementById('app-body'));

    toastr.options = {
      "closeButton": true,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-top-center",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }
});