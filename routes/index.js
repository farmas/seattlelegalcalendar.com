var express = require('express');
var router = express.Router();
var auth = require("./auth");
var calendar = require("./calendar");


router.get('/', function(req, res, next) {
    var calendarId = process.env.GOOGLE_CALENDAR_ID || 'seattlelegalcalendar@gmail.com';
    var calendarName = process.env.GOOGLE_CALENDAR_NAME || 'Seattle Legal Events';

    res.render('index', {
        calendarName: encodeURIComponent(calendarName),
        calendarId: encodeURIComponent(calendarId),
        username: req.user && req.user.name,
        errors: req.flash("error")
      });
});

router.use("/auth", auth);
router.use("/calendar", calendar);

module.exports = router;
