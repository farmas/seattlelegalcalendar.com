var express = require('express');
var router = express.Router();

var GoogleTokenProvider = require('refresh-token').GoogleTokenProvider;
var request = require('request');
var moment = require('moment');

var tokenProvider = new GoogleTokenProvider({
    refresh_token: process.env.GOOGLE_REFRESH_TOKEN,
    client_id: process.env.GOOGLE_CLIENT_ID,
    client_secret: process.env.GOOGLE_CLIENT_SECRET
});

var calendarId = process.env.GOOGLE_CALENDAR_ID || 'seattlelegalcalendar@gmail.com';
var calendarEventsUrl = 'https://www.googleapis.com/calendar/v3/calendars/' + calendarId + '/events';
var timezonePST = 'America/Los_Angeles';

router.use(function(req, res, next) {
    if (!req.user) {
        res.status(403).send('Unauthorized');
    } else {
        next();
    }
});

router.post('/addEvent', function(req, res, next) {
    req.checkBody('summary', 'Summary is required.').notEmpty();
    req.checkBody('startTime', 'Start time is required.').notEmpty().isNumeric();
    req.checkBody('endTime', 'End time is required.').notEmpty().isNumeric();

    var debug = !!req.query.debug;
    var errors = req.validationErrors();
    var startDate = new Date(parseInt(req.body.startTime, 0));
    var endDate = new Date(parseInt(req.body.endTime, 0));
    var includeUserInfo = req.sanitizeBody('includeUserInfo').toBoolean();

    var event = {
        summary: req.body.summary,
        description: req.body.description,
        attendees: [
            { email: 'seattlelegalcalendar@gmail.com' }
        ],
        start:
        {
            dateTime: moment(startDate).format("YYYY-MM-DDTHH:mm:ssZ"),
            timeZone: timezonePST
        },
        end:
        {
            dateTime: moment(endDate).format("YYYY-MM-DDTHH:mm:ssZ"),
            timeZone: timezonePST
        }
    };

    if (includeUserInfo) {
        event.attendees.push({ email: req.user.email, displayName: req.user.name });
        event.organizer = {
            id: req.user.id,
            displayName: req.user.name
        };
    }

    debug && console.log('DEBUG MODE');
    console.log('event to add', event);

    if (errors) {
        res.status(400).send(errors[0].msg);
    } else if (!debug) {
        executeOAuthRequest(calendarEventsUrl, event, next, function(body) {
            res.setHeader('Content-Type', 'application/json');
            res.send({
                id: body.id,
                link: body.htmlLink
            });
        });
    } else {
        res.setHeader('Content-Type', 'application/json');
        res.send({
            id: 'test-id',
            link: 'test-link'
        });
    }
});

router.get('/getEvents', function(req, res, next) {
    executeOAuthRequest(calendarEventsUrl, null, next, function(body) {
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.parse(body).items);
    });
});

function executeOAuthRequest(url, body, next, successCallback) {
    var requestOptions = {
        url: url,
        json: true,
        auth: {
            'bearer': null
        }
    };

    if (body) {
        requestOptions.method = 'POST',
        requestOptions.body = body;
    }

    tokenProvider.getToken(function (err, token) {
        if (err) {
            console.log('Failed to retrieve token', err);
            next(err);
        } else {
            requestOptions.auth.bearer = token;
            request(requestOptions, function (error, response, body) {
                if (error) {
                    console.log('Failed to send request', error);
                    next(error);
                } else if (response.statusCode !== 200) {
                    console.log('Response is not have a 200 OK.', response.statusCode, body);
                    next(new Error('Invalid status code.'));
                } else {
                    successCallback(body, response);
                }
            });
        }
    });
}

module.exports = router;
