## About ##

Source code for the Seattle Legal Calendar website. It displays a shared calendar where authenticated users can create events related the legal profession in the Seattle area.

## Getting started for developers ##

* Set the following environment variables to your google client id and client secret: GOOGLE_CLIENT_ID, GOOGLE_CLIENT_SECRET.
* npm install
* npm run dev
* Navigate to localhost:3000 to load the site.

## Contact ##
[seattlelegalcalendar@gmail.com](seattlelegalcalendar@gmail.com)
